#!/bin/bash

if [[ "$DEVELOPMENT" == "true" ]]; then
    exec gunicorn plyn.app:app \
        --bind 0.0.0.0:8000 \
        --workers 1 \
        --worker-class eventlet \
        --reload
else
    exec gunicorn plyn.app:app \
        --bind 0.0.0.0:8000 \
        --worker-class eventlet \
        --workers 1
fi

"$@"
