registry=registry.gitlab.com/plyn/server-python
app_name=plyn
builder=docker

image_name=${registry}/${app_name}-server

all: image

.PHONY: build-dev run-dev image push

build-dev:
	docker-compose -f docker/docker-compose-development.yaml build

run-dev:
	docker-compose -f docker/docker-compose-development.yaml up

image:
	${builder} build -t ${image_name} --file docker/Dockerfile .

push: image
	${builder} push ${image_name}
