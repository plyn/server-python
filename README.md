Collaborative text editing server in python

# Usage

## Install dependencies

Redis have to be installed as well as python3.
On debian, it can be installed using `apt install redis-server python3` have to be installed.

## Launch

Launch the server in development mode.

```
make run-dev
```

It requires `pipenv` to be installed
If you can't install pipenv, you will have to install manually python dependencies listed in `Pipfile[packages]` in a virtualenv.
