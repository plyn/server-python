import walrus
from uuid import uuid4

from plyn.config import config

class Redis():

    users = None

    def __init__(self):

        db_config = config['database']

        self.redis = walrus.Walrus(
                        host=db_config['host'],
                        port=db_config['port'],
                        password=db_config['password'],
                        db=db_config['db'])

        self.users = self.redis.List('users')

    def cursor(self, uuid: str):
        """ return the cursor of a ®®
        """
        return self.redis.Hash('users:' + uuid + ':cursor')


r = Redis()

class Cursor():

    def __init__(self, uuid):
        self.cursor = r.cursor(uuid)
        self.uuid = uuid

    @property
    def line(self) -> int:
        return self.cursor['line']

    @line.setter
    def line(self, line: int):
        self.cursor['line'] = line

    @property
    def column(self) -> int:
        return self.cursor['column']

    @column.setter
    def column(self, column: int):
        self.cursor['column'] = column

    def update(self, line: int, column: int) -> dict:
        """ update cursor in redis

        Returns an update event

        :param line: new line of the selected cursor
        """
        prev_line, prev_column = self.line, self.column
        self.line = line
        self.column = column
        if prev_line != line or prev_column != column:
            return {'cursor': {'line': line, 'column': column}}
        return {}

    def __str__(self):
        return 'line: {}, column: {}'.format(self.line, self.column)

    def __repr__(self):
        return 'Cursor({})'.format(str(self))



class User():

    def __init__(self, uuid):

        self.uuid = uuid
        self.cursor = Cursor(uuid)

    def update(self, update: dict) -> dict:
        """ Update user information according to data

        Only cursor position is taken into account. Other operations are
        ignored.

        It return changes made

        # Example
        >>> user.update({'cursor': {'line': 3, 'column': 3}})

        :param data: dictionnary containing cursor position
        """
        changes: dict = {}
        if 'cursor' in update:
            line = update['cursor']['line']
            column = update['cursor']['column']
            changes = self.cursor.update(line, column)
        return changes

    def __str__(self):
        return 'uuid: {}, cursor: ({!s})'.format(self.uuid, self.cursor)

    def __repr__(self):
        return 'User({})'.format(str(self))

