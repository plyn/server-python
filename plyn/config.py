import os
import toml

file_dir = os.path.dirname(os.path.realpath(__file__))
config = toml.load(os.path.join(file_dir, "config.toml"))
