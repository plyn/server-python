import logging
import socketio
import eventlet

from plyn.database import User
from plyn.config import config


if __name__ != '__main__':
    logger = logging.getLogger('gunicorn.error')
else:
    logger = logging.Logger(__name__)

sio = socketio.Server(logger=logger)
app = socketio.WSGIApp(sio)


@sio.on('join')
def join(sid, data):
    # username = data['username'] if 'username' in data else generate_username()
    # id = request.sid
    logger.info("User {} joined".format(sid))
    sio.enter_room(sid, 'main')


@sio.on('update')
def update(sid, data):
    """ handle 'update' events received from a user

    It is used to update both text and user information
    """
    logger.info("Update event received")

    uuid = sid
    user_changes = User(uuid).update(data)
    # TODO: use Text object : Text('text_id').update(data)
    text_changes = ({'operation': data['operation']}
                    if 'operation' in data
                    else {})

    changes = {**user_changes, **text_changes}
    if changes:
        sio.emit('update',
                      {'uuid': uuid, **changes},
                      skip_sid=sid,
                      room='main')


if __name__ == '__main__':
    eventlet.wsgi.server(eventlet.listen(('', 5000)), app)

